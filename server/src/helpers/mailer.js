const nodemailer = require('nodemailer');

module.exports = {
  createMailTransporter: () => nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASSWORD
    }
  }),

  send: ({ transporter, mailOptions }) => {
    transporter.sendMail(mailOptions, err => {
      if (err) {
        return Promise.reject({ message: 'Something went wrong while sending you an email, please try again...' });
      }
      return false;
    });
  }
};
