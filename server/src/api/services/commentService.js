import commentRepository from '../../data/repositories/commentRepository';
import CommentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const getCommentReaction = (userId, commentId) => (
  CommentReactionRepository.getCommentReaction(userId, commentId)
);

export const updateCommentById = (commentId, { body, edited }) => (
  commentRepository.updateById(commentId, { body, edited })
);

export const deleteCommentById = async commentId => {
  const result = await commentRepository.deleteById(commentId);

  return Number.isInteger(result) ? {} : result;
};

export const getCommentReactionUsernames = commentId => (
  CommentReactionRepository.getCommentReactionUsernames(commentId)
);

export const setReaction = async (userId, { commentId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? CommentReactionRepository.deleteById(react.id)
    : CommentReactionRepository.updateById(react.id, { isLike }));

  const reaction = await CommentReactionRepository.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await CommentReactionRepository.create({ userId, commentId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : CommentReactionRepository.getCommentReaction(userId, commentId);
};
