import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image, status } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, status };
};

export const updateUsername = async (userId, { username }) => {
  const user = await userRepository.getByUsername(username);

  if (user) {
    return Promise.reject({ message: `Username ${username} is already taken` });
  }

  await userRepository.updateById(userId, { username });
  return userRepository.getUserById(userId);
};

export const updateUserImageId = (userId, { imageId }) => userRepository.updateById(userId, { imageId });
export const updateUserStatus = (userId, { status }) => userRepository.updateById(userId, { status });
