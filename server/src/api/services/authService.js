import mailer from '../../helpers/mailer';
import { createToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import userRepository from '../../data/repositories/userRepository';

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password)
  });
  return login(newUser);
};

const getMailOptions = (email, emailBody) => ({
  to: email,
  from: `"Thread 🧶" <${process.env.EMAIL_ADDRESS}>`,
  subject: 'Thread Password Reset',
  text: emailBody
});

export const resetPassword = async ({ email }) => {
  const token = createToken({ email });
  const transporter = mailer.createMailTransporter();
  const emailBody = `
  You are receiving this because you (or someone else) have requested the reset of the password for your account.
  Please click on the following link, or paste this into your browser to complete the process:

  http://localhost:3000/reset-password/${token}

  If you did not request this, please ignore this email and your password will remain unchanged.`;

  const mailOptions = getMailOptions(email, emailBody);
  const user = await userRepository.getByEmail(email);

  if (!user) {
    return Promise.reject({ message: `Didn't find any account with ${email} email address` });
  }

  const resetData = {
    resetPasswordToken: token,
    resetPasswordExpires: Date.now() + 3600000 // 1 hour
  };

  await userRepository.updateById(user.id, resetData);
  mailer.send({ transporter, mailOptions });

  return { message: `Email with reset link has been successfully sent to ${email}` };
};

export const updatePassword = async (token, { password }) => {
  const transporter = mailer.createMailTransporter();
  const user = await userRepository.getByResetToken(token);

  if (!user) {
    return Promise.reject({ message: 'Password reset token is invalid or has expired.' });
  }

  const encryptedPassword = await encrypt(password);
  const resetData = {
    resetPasswordToken: null,
    resetPasswordExpires: null
  };

  await userRepository.updateById(user.id, { ...resetData, password: encryptedPassword });

  const emailBody = `
  Hello

  This is a confirmation that the password
  for your account ${user.email} has just been changed.`;

  const mailOptions = getMailOptions(user.email, emailBody);
  mailer.send({ transporter, mailOptions });

  return { message: 'Success! Your password has been changed.' };
};
