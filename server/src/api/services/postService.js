import mailer from '../../helpers/mailer';
import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const getPostReaction = (userId, postId) => postReactionRepository.getPostReaction(userId, postId);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const updatePostById = (postId, { body, edited }) => postRepository.updateById(postId, { body, edited });

export const deletePostById = async postId => {
  const result = await postRepository.deleteById(postId);

  return Number.isInteger(result) ? {} : result;
};

export const getPostReactionUsernames = postId => postReactionRepository.getPostReactionUsernames(postId);

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const shareByEmail = async (username, { email, link }) => {
  const transporter = mailer.createMailTransporter();
  const emailBody = `
  Hello.

  ${username} shared a post with you.

  Take a look: ${link}`;

  const mailOptions = {
    to: email,
    from: `"Thread 🧶" <${process.env.EMAIL_ADDRESS}>`,
    subject: 'Check out post shared by...',
    text: emailBody
  };
  mailer.send({ transporter, mailOptions });

  return { message: `Post was successfully shared with ${email}` };
};
