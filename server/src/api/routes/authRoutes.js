import { Router } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/register', registrationMiddleware, (req, res, next) => authService.register(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/reset-password', (req, res, next) => authService.resetPassword(req.body)
    .then(data => res.send(data))
    .catch(next))
  .post('/reset-password/:token', (req, res, next) => authService.updatePassword(req.params.token, req.body)
    .then(data => res.send(data))
    .catch(next))
  .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById(req.user.id)
    .then(data => res.send(data))
    .catch(next))
  .put('/user', (req, res, next) => userService.updateUsername(req.user.id, req.body)
    .then(user => res.send(user))
    .catch(next))
  .put('/user/image', (req, res, next) => userService.updateUserImageId(req.user.id, req.body)
    .then(user => res.send(user))
    .catch(next))
  .put('/user/status', (req, res, next) => userService.updateUserStatus(req.user.id, req.body)
    .then(user => res.send(user))
    .catch(next));

export default router;
