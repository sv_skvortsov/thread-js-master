import { Router } from 'express';
import * as postService from '../services/postService';
import mailer from '../../helpers/mailer';

const router = Router();
const transporter = mailer.createMailTransporter();
const getMailOptions = (email, emailBody) => ({
  to: email,
  from: `"Thread 🧶" <${process.env.EMAIL_ADDRESS}>`,
  subject: 'Your post was liked by...',
  text: emailBody
});

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .get('/react/usernames/:id', (req, res, next) => postService.getPostReactionUsernames(req.params.id)
    .then(usernames => res.send(usernames))
    .catch(next))
  .get('/react/:id', (req, res, next) => postService.getPostReaction(req.query.userId, req.params.id)
    .then(post => res.send(post || {}))
    .catch(next))
  .post('/share', (req, res, next) => postService.shareByEmail(req.user.username, req.body)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && reaction.isLike && (reaction.post.userId !== req.user.id)) {
        const { email } = reaction.post.user;
        const emailBody = `
        Hello.

        Your post was liked by ${req.user.username}.
        
        Post: ${req.headers.referer}share/${reaction.post.id}`;

        const mailOptions = getMailOptions(email, emailBody);

        // notify a user if someone (not himself) liked his post
        mailer.send({ transporter, mailOptions });
        req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
      }
      if (reaction.post && !reaction.isLike && (reaction.post.userId !== req.user.id)) {
        // notify a user if someone (not himself) disliked his post
        req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked!');
      }
      if (reaction.post) {
        req.io.emit('reaction', reaction.post.id);
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => postService.updatePostById(req.params.id, req.body)
    .then(post => {
      req.io.emit('post_updated', post.id);
      return res.send(post);
    })
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deletePostById(req.params.id)
    .then(post => {
      req.io.emit('post_deleted', post.id);
      return res.send(post);
    })
    .catch(next));

export default router;
