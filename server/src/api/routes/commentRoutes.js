import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .get('/react/usernames/:id', (req, res, next) => commentService.getCommentReactionUsernames(req.params.id)
    .then(usernames => res.send(usernames))
    .catch(next))
  .get('/react/:id', (req, res, next) => commentService.getCommentReaction(req.query.userId, req.params.id)
    .then(comment => res.send(comment || {}))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.comment && reaction.isLike && (reaction.comment.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his comment
        req.io.to(reaction.comment.userId).emit('like', 'Your comment was liked!');
      }
      if (reaction.comment && !reaction.isLike && (reaction.comment.userId !== req.user.id)) {
        // notify a user if someone (not himself) disliked his comment
        req.io.to(reaction.comment.userId).emit('dislike', 'Your comment was disliked!');
      }
      if (reaction.comment) {
        req.io.emit('comment_reaction');
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => commentService.updateCommentById(req.params.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next));

export default router;
