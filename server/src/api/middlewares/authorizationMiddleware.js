import { pathToRegexp } from 'path-to-regexp';
import jwtMiddleware from './jwtMiddleware';

export default (routesWhiteList = []) => (req, res, next) => (
  routesWhiteList.some(route => {
    const regexp = pathToRegexp(route);
    return regexp.test(req.path);
  })
    ? next()
    : jwtMiddleware(req, res, next) // auth the user if requested path isn't from the white list
);
