export default (orm, DataTypes) => {
  const User = orm.define('user', {
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    username: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    status: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    resetPasswordToken: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    resetPasswordExpires: {
      allowNull: true,
      type: DataTypes.DATE,
      defaultValue: null
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return User;
};
