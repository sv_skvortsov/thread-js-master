import { CommentReactionModel, CommentModel, UserModel } from '../models/index';
import BaseRepository from './baseRepository';
import sequelize from '../db/connection';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: { userId, commentId },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getCommentReactionUsernames(commentId) {
    return this.model.findAll({
      group: [
        'commentReaction.id',
        'user.id'
      ],
      where: { commentId, isLike: true },
      attributes: [
        [sequelize.fn('', sequelize.col('user.username')), 'username']
      ],
      include: [{
        model: UserModel,
        attributes: []
      }]
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
