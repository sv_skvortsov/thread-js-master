import sequelize, { op } from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  CommentReactionModel,
  PostReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = (bool, showLikedByMe) => {
  if (showLikedByMe) {
    return (`
      (SELECT COUNT(*)
      FROM "postReactions" as "postReaction"
      WHERE "postReaction"."postId" = "post"."id"
      AND "postReaction"."isLike" = ${bool})`);
  }
  return `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
};
const commentLikeCase = bool => (`
  (SELECT COUNT(*)
  FROM "commentReactions" as "commentReaction"
  WHERE "commentReaction"."commentId" = "comments"."id"
  AND "commentReaction"."isLike" = ${bool})
`);

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      showOwnPosts,
      hideMyPosts,
      showLikedByMe
    } = filter;

    const where = {};

    if (showOwnPosts && !hideMyPosts && userId) {
      Object.assign(where, { userId });
    }
    if (hideMyPosts && !showOwnPosts && userId) {
      Object.assign(where, { userId: { [op.ne]: userId } });
    }
    if (userId && showLikedByMe) {
      Object.assign(where, {
        [op.and]: [
          { '$postReactions.userId$': userId },
          { '$postReactions.isLike$': true }
        ]
      });
    }
    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true, showLikedByMe))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false, showLikedByMe))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'comments->commentReactions.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        include: {
          model: UserModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }, {
        model: CommentModel,
        attributes: {
          include: [
            [sequelize.literal(commentLikeCase(true)), 'likeCount'],
            [sequelize.literal(commentLikeCase(false)), 'dislikeCount']
          ]
        },
        include: {
          model: CommentReactionModel,
          attributes: []
        }
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }
}

export default new PostRepository(PostModel);
