import { PostReactionModel, PostModel, UserModel } from '../models/index';
import BaseRepository from './baseRepository';
import sequelize from '../db/connection';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id',
        'post->user.id'
      ],
      where: { userId, postId },
      include: [{
        model: PostModel,
        attributes: ['id', 'userId'],
        include: [{
          model: UserModel,
          attributes: ['email']
        }]
      }]
    });
  }

  getPostReactionUsernames(postId) {
    return this.model.findAll({
      group: [
        'postReaction.id',
        'user.id'
      ],
      where: { postId, isLike: true },
      attributes: [
        [sequelize.fn('', sequelize.col('user.username')), 'username']
      ],
      include: [{
        model: UserModel,
        attributes: []
      }]
    });
  }
}

export default new PostReactionRepository(PostReactionModel);
