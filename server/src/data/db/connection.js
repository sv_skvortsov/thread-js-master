import Sequelize from 'sequelize';
import * as config from '../../config/dbConfig';

export const op = Sequelize.Op;

export default new Sequelize(config);
