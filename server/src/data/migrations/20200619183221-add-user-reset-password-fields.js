export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('users', 'resetPasswordToken', {
        allowNull: true,
        type: Sequelize.STRING,
        defaultValue: null
      }, { transaction }),
      queryInterface.addColumn('users', 'resetPasswordExpires', {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: null
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'resetPasswordToken', { transaction }),
      queryInterface.removeColumn('users', 'resetPasswordExpires', { transaction })
    ]))
};
