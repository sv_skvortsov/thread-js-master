export default {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('posts', 'edited', {
    allowNull: false,
    type: Sequelize.BOOLEAN,
    defaultValue: false
  }),
  down: queryInterface => queryInterface.removeColumn('posts', 'edited')
};

