export default {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('comments', 'edited', {
    allowNull: false,
    type: Sequelize.BOOLEAN,
    defaultValue: false
  }),
  down: queryInterface => queryInterface.removeColumn('comments', 'edited')
};

