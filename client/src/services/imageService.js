import callWebApi from 'src/helpers/webApiHelper';

export const uploadImage = async image => {
  const response = await callWebApi({
    endpoint: '/api/images',
    type: 'POST',
    attachment: image
  });
  return response.json();
};

export const updateProfilePhoto = async (imageId, link) => {
  const response = await callWebApi({
    endpoint: `/api/images/${imageId}`,
    type: 'PUT',
    request: {
      link
    }
  });
  return response.json();
};
