import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const resetPassword = async email => {
  const response = await callWebApi({
    endpoint: '/api/auth/reset-password',
    type: 'POST',
    request: {
      email
    }
  });
  return response.json();
};

export const updatePassword = async (token, password) => {
  const response = await callWebApi({
    endpoint: `/api/auth/reset-password/${token}`,
    type: 'POST',
    request: {
      password
    }
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const updateUsername = async username => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'PUT',
      request: {
        username
      }
    });
    return response.json().then(res => [null, res]);
  } catch (e) {
    return [e.message, undefined];
  }
};

export const updateUserImageId = async imageId => {
  const response = await callWebApi({
    endpoint: '/api/auth/user/image',
    type: 'PUT',
    request: {
      imageId
    }
  });
  return response.json();
};

export const updateUserStatus = async status => {
  const response = await callWebApi({
    endpoint: '/api/auth/user/status',
    type: 'PUT',
    request: {
      status
    }
  });
  return response.json();
};
