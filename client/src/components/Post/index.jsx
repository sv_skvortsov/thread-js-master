import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Form, TextArea, Button, Popup, Header, Placeholder } from 'semantic-ui-react';
import moment from 'moment';
import { getPostReaction, getPostReactionUsernames } from '../../services/postService';

import styles from './styles.module.scss';

const popupStyle = {
  minWidth: 100,
  borderRadius: 4,
  opacity: 0.9
};

const popupPlaceholder = (
  <Placeholder inverted style={{ minWidth: '100px' }}>
    <Placeholder.Header>
      <Placeholder.Line length="short" />
    </Placeholder.Header>
    <Placeholder.Paragraph>
      <Placeholder.Line length="very long" />
    </Placeholder.Paragraph>
  </Placeholder>
);

const Post = ({
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  sharePost,
  updatePost,
  deletePost,
  currentUserId,
  expandedPost
}) => {
  const {
    id,
    image,
    body,
    edited,
    user: owner,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  const inputRef = useRef(null);
  const [isLikedByMe, setLikedByMe] = useState(null);
  const [isEditMode, setEditMode] = useState(false);
  const [bodyValue, setBodyValue] = useState(body);
  const [usernamesList, setUsernamesList] = useState(null);

  const fetchPostReaction = async () => {
    const postReaction = await getPostReaction(id, currentUserId);

    if (Object.keys(postReaction).length) {
      setLikedByMe(postReaction.isLike);
    } else {
      setLikedByMe(null);
    }
  };

  useEffect(() => {
    fetchPostReaction();
  });

  useEffect(() => {
    setBodyValue(body);
  }, [body]);

  useEffect(() => {
    if (inputRef && inputRef.current) {
      inputRef.current.focus();
    }
  }, [isEditMode]);

  const editPost = () => {
    setEditMode(true);
  };

  const handleEditCancel = () => {
    setEditMode(false);
    setBodyValue(body);
  };

  const handleEditSave = () => {
    const trimmedValue = bodyValue.trim();
    setEditMode(false);
    if (body !== trimmedValue) {
      updatePost(id, trimmedValue, true);
      return;
    }
    setBodyValue(body);
  };

  const likeComponent = (
    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
      <Icon {...(isLikedByMe && { color: 'blue' })} name="thumbs up" />
      {likeCount}
    </Label>
  );

  const getUsernamesList = async () => {
    const usernames = await getPostReactionUsernames(id);
    const result = usernames.map(obj => obj.username);
    setUsernamesList(result);
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">{`posted by ${owner.username} - ${date} ${edited ? '(edited)' : ''}`}</span>
          {currentUserId === owner.id && !expandedPost && (
            <div>
              <Icon name="pencil" className={styles.editIcon} onClick={editPost} />
              <Icon name="delete" className={styles.deleteIcon} onClick={() => deletePost(id)} />
            </div>
          )}
        </Card.Meta>
        <Card.Description>
          {isEditMode
            ? (
              <Form>
                <div className={styles.textareaWrapper}>
                  <TextArea
                    ref={inputRef}
                    value={bodyValue}
                    onChange={e => setBodyValue(e.target.value)}
                  />
                </div>
                <Button
                  compact
                  floated="right"
                  color="blue"
                  onClick={handleEditSave}
                >
                  Save
                </Button>
                <Button
                  compact
                  floated="right"
                  color="red"
                  onClick={handleEditCancel}
                >
                  Cancel
                </Button>
              </Form>
            )
            : bodyValue}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          trigger={likeComponent}
          onOpen={() => getUsernamesList()}
          style={popupStyle}
          inverted
          disabled={likeCount <= 0}
          mouseEnterDelay={250}
          mouseLeaveDelay={250}
          popperDependencies={[!!usernamesList]}
          size="tiny"
        >
          {usernamesList ? (
            <>
              <Header as="h2" content="Liked by:" />
              <p>{usernamesList.join(', ')}</p>
            </>
          ) : popupPlaceholder}
        </Popup>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon {...(!isLikedByMe && isLikedByMe !== null && { color: 'red' })} name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func,
  deletePost: PropTypes.func,
  currentUserId: PropTypes.string.isRequired,
  expandedPost: PropTypes.bool
};

Post.defaultProps = {
  expandedPost: false,
  updatePost: () => {},
  deletePost: () => {}
};

export default Post;
