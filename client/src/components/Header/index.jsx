import React, { useState, useEffect, useRef } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button, Input } from 'semantic-ui-react';
import { updateUserStatus } from '../../services/authService';

import styles from './styles.module.scss';

const Header = ({ user, logout }) => {
  const [editStatus, setEditStatus] = useState(false);
  const [status, setStatus] = useState(user.status || '');
  const statusInputRef = useRef(null);

  useEffect(() => {
    if (statusInputRef && statusInputRef.current) {
      statusInputRef.current.focus();
    }
  }, [editStatus]);

  const handleEditCancel = () => {
    setEditStatus(false);
    setStatus(user.status);
  };

  const handleEditSave = () => {
    const trimmedStatus = status.trim();
    setEditStatus(false);

    if (user.status !== trimmedStatus) {
      updateUserStatus(trimmedStatus);
      return;
    }
    setStatus(user.status);
  };

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <NavLink exact to="/">
              <HeaderUI>
                <Image circular src={getUserImgLink(user.image)} />
                {' '}
                <div className={styles.headerUserInfo}>
                  {user.username}
                  <div className={styles.statusWrapper}>
                    <HeaderUI.Subheader>
                      {editStatus ? (
                        <Input
                          transparent
                          ref={statusInputRef}
                          value={status}
                          onChange={e => setStatus(e.target.value)}
                          disabled={!editStatus}
                        />
                      ) : <span>{status}</span>}
                      {editStatus && status.trim() !== user.status ? (
                        <>
                          <Button
                            compact
                            size="mini"
                            color="blue"
                            style={{ marginLeft: 10 }}
                            onClick={handleEditSave}
                          >
                            Save
                          </Button>
                          <Button
                            compact
                            size="mini"
                            color="red"
                            onClick={handleEditCancel}
                          >
                            Cancel
                          </Button>
                        </>
                      ) : (
                        <Icon
                          name="pencil"
                          className={cx(styles.editIcon, { [styles.hide]: !status })}
                          onClick={() => setEditStatus(true)}
                        />
                      )}
                    </HeaderUI.Subheader>
                  </div>
                </div>
              </HeaderUI>
            </NavLink>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
