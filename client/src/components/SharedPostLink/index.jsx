import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Modal, Input, Icon, Button, Message } from 'semantic-ui-react';
import { sharePostByEmail } from '../../services/postService';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close }) => {
  const [copied, setCopied] = useState(false);
  const [email, setEmail] = useState('');
  const [showEmailField, setShowEmailField] = useState(false);
  const [isEmailValid, setEmailValid] = useState(true);
  const [isLoading, setLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState('');
  const [error, setError] = useState('');
  const inputRef = useRef();

  const copyToClipboard = e => {
    inputRef.select();
    document.execCommand('copy');
    e.target.focus();
    setCopied(true);
  };

  const handleEmailBlur = () => {
    const validateEmail = validator.isEmail(email);

    if (!validateEmail) {
      setError('Wrong email format');
    }

    setEmailValid(validateEmail);
  };

  const shareByEmail = async () => {
    const link = `${window.location.origin}/share/${postId}`;
    if (!isEmailValid) {
      return;
    }

    setLoading(true);

    try {
      const { message } = await sharePostByEmail(email, link);
      setSuccessMessage(message);
    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={inputRef}
        />
        <div className={styles.shareBy}>
          Share by:
          <Button icon className={styles.emailButton} onClick={() => setShowEmailField(true)}>
            <Icon name="mail" />
          </Button>
        </div>
        {showEmailField && !successMessage && (
          <Input
            fluid
            className={styles.shareBy}
            action={{
              color: 'blue',
              labelPosition: 'right',
              icon: 'mail',
              content: 'Send',
              disabled: !isEmailValid,
              loading: isLoading,
              onClick: shareByEmail
            }}
            value={email}
            error={!isEmailValid}
            placeholder="Type an email for the post share"
            onChange={e => setEmail(e.currentTarget.value)}
            onBlur={handleEmailBlur}
          />
        )}
        {!isEmailValid && (
          <Message
            negative
            content={error}
          />
        )}
        {successMessage && (
          <Message
            positive
            content={successMessage}
          />
        )}
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default SharedPostLink;
