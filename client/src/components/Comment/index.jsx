import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import {
  Comment as CommentUI,
  Form,
  TextArea,
  Button,
  Icon,
  Label,
  Popup,
  Header,
  Placeholder
} from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { getCommentReaction, getCommentReactionUsernames } from '../../services/commentService';

import styles from './styles.module.scss';

const popupStyle = {
  minWidth: 100,
  borderRadius: 4,
  opacity: 0.9
};

const popupPlaceholder = (
  <Placeholder inverted style={{ minWidth: '100px' }}>
    <Placeholder.Header>
      <Placeholder.Line length="short" />
    </Placeholder.Header>
    <Placeholder.Paragraph>
      <Placeholder.Line length="very long" />
    </Placeholder.Paragraph>
  </Placeholder>
);

const Comment = ({
  comment: {
    id,
    body,
    createdAt,
    user,
    edited,
    likeCount,
    dislikeCount
  },
  currentUserId,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment
}) => {
  const inputRef = useRef(null);
  const [isEditMode, setEditMode] = useState(false);
  const [isLikedByMe, setLikedByMe] = useState(null);
  const [bodyValue, setBodyValue] = useState(body);
  const [usernamesList, setUsernamesList] = useState(null);

  useEffect(() => {
    if (inputRef && inputRef.current) {
      inputRef.current.focus();
    }
  }, [isEditMode]);

  const fetchCommentReaction = async () => {
    const commentReaction = await getCommentReaction(id, currentUserId);

    if (Object.keys(commentReaction).length) {
      setLikedByMe(commentReaction.isLike);
    } else {
      setLikedByMe(null);
    }
  };

  useEffect(() => {
    fetchCommentReaction();
  });

  useEffect(() => {
    setBodyValue(body);
  }, [body]);

  const editComment = () => {
    setEditMode(true);
  };

  const handleEditCancel = () => {
    setEditMode(false);
    setBodyValue(body);
  };

  const handleEditSave = () => {
    const trimmedValue = bodyValue.trim();
    setEditMode(false);
    if (body !== trimmedValue) {
      updateComment(id, trimmedValue, true);
      return;
    }
    setBodyValue(body);
  };

  const likeComponent = (
    <Label size="small" basic as="a" className={styles.reactionBtn} onClick={() => likeComment(id)}>
      <Icon {...(isLikedByMe && { color: 'blue' })} name="thumbs up" />
      {likeCount}
    </Label>
  );

  const getUsernamesList = async () => {
    const usernames = await getCommentReactionUsernames(id);
    const result = usernames.map(obj => obj.username);
    setUsernamesList(result);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {`${moment(createdAt).fromNow()} ${edited ? '(edited)' : ''}`}
          <div className={styles.metadataContent}>
            {currentUserId === user.id && (
              <div>
                <Icon name="pencil" className={styles.editIcon} onClick={editComment} />
                <Icon name="delete" className={styles.deleteIcon} onClick={() => deleteComment(id)} />
              </div>
            )}
          </div>
        </CommentUI.Metadata>
        <p className={styles.status}>{user.status}</p>
        <CommentUI.Text>
          {isEditMode
            ? (
              <Form>
                <div className={styles.textareaWrapper}>
                  <TextArea
                    ref={inputRef}
                    value={bodyValue}
                    onChange={e => setBodyValue(e.target.value)}
                  />
                </div>
                <Button
                  compact
                  floated="right"
                  color="blue"
                  onClick={handleEditSave}
                >
                  Save
                </Button>
                <Button
                  compact
                  floated="right"
                  color="red"
                  onClick={handleEditCancel}
                >
                  Cancel
                </Button>
              </Form>
            )
            : bodyValue}
        </CommentUI.Text>
        <Popup
          trigger={likeComponent}
          onOpen={() => getUsernamesList()}
          style={popupStyle}
          inverted
          disabled={likeCount <= 0}
          mouseEnterDelay={250}
          mouseLeaveDelay={250}
          popperDependencies={[!!usernamesList]}
          position="bottom center"
          size="tiny"
        >
          {usernamesList ? (
            <>
              <Header as="h2" content="Liked by:" />
              <p>{usernamesList.join(', ')}</p>
            </>
          ) : popupPlaceholder}
        </Popup>
        <Label size="small" basic as="a" className={styles.reactionBtn} onClick={() => dislikeComment(id)}>
          <Icon {...(!isLikedByMe && isLikedByMe !== null && { color: 'red' })} name="thumbs down" />
          {dislikeCount}
        </Label>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  currentUserId: PropTypes.string.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

export default Comment;
