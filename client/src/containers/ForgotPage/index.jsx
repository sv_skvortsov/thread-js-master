import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { resetPassword } from 'src/services/authService';
import Logo from 'src/components/Logo';
import { Form, Message, Segment, Button, Grid, Header } from 'semantic-ui-react';

import styles from './styles.module.scss';

const ForgotPage = ({ history }) => {
  const [email, setEmail] = useState('');
  const [error, setError] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const [isEmailValid, setEmailValid] = useState(true);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      if (successMessage) {
        history.push('/');
      }
    }, 3000);
  }, [successMessage]);

  const emailChanged = value => {
    setEmail(value);
    setError('');
    setEmailValid(true);
  };

  const register = async () => {
    if (!isEmailValid || isLoading) {
      return;
    }
    setLoading(true);
    try {
      const { message } = await resetPassword(email);
      setSuccessMessage(message);
    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const handleEmailInputBlur = () => {
    const validateEmail = validator.isEmail(email);

    if (!validateEmail) {
      setError('Email format is incorrect');
    } else {
      setError('');
    }

    setEmailValid(validator.isEmail(email));
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center" style={{ display: 'block' }}>
          Forgot your password?
        </Header>
        <Form error={Boolean(error)} name="forgotPasswordForm" size="large" onSubmit={register}>
          <Segment>
            <p className={styles.formLabel}>
              Enter your email address below, and we&apos;ll send you an email allowing you to reset it.
            </p>
            <Form.Input
              fluid
              icon="at"
              iconPosition="left"
              placeholder="Email"
              type="email"
              error={Boolean(error) || !isEmailValid}
              onChange={e => emailChanged(e.target.value)}
              onBlur={handleEmailInputBlur}
            />
            <Message
              error
              content={error}
            />
            {successMessage && (
              <Message
                positive
                content={successMessage}
              />
            )}
            {!successMessage && (
              <Button
                fluid
                primary
                type="submit"
                color="teal"
                size="large"
                loading={isLoading}
                disabled={!email || !isEmailValid || isLoading}
              >
                Reset Password
              </Button>
            )}
          </Segment>
        </Form>
      </Grid.Column>
    </Grid>
  );
};

ForgotPage.propTypes = {
  history: PropTypes.objectOf(PropTypes.any)
};

ForgotPage.defaultProps = {
  history: undefined
};

export default ForgotPage;
