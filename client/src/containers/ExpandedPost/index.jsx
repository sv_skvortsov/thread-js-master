import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import io from 'socket.io-client';
import {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteCommentById,
  likeComment,
  dislikeComment,
  loadExpendedPost
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  userId,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  updateComment: updatePostComment,
  deleteCommentById: deletePostComment,
  likeComment: likePostComment,
  dislikeComment: dislikePostComment,
  loadExpendedPost: updatedExpendedPost
}) => {
  useEffect(() => {
    const { REACT_APP_SOCKET_SERVER: address } = process.env;
    const socket = io(address);

    socket.on('comment_reaction', () => updatedExpendedPost(post.id));

    return () => {
      socket.close();
    };
  });

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              likePost={like}
              dislikePost={dislike}
              toggleExpandedPost={toggle}
              sharePost={sharePost}
              currentUserId={userId}
              expandedPost
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    currentUserId={userId}
                    updateComment={updatePostComment}
                    deleteComment={deletePostComment}
                    likeComment={likePostComment}
                    dislikeComment={dislikePostComment}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteCommentById: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  loadExpendedPost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteCommentById,
  likeComment,
  dislikeComment,
  loadExpendedPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
