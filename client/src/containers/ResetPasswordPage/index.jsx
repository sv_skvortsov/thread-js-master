import React, { useState, useEffect } from 'react';
import { updatePassword } from 'src/services/authService';
import PropTypes from 'prop-types';
import Logo from 'src/components/Logo';
import { Form, Message, Segment, Button, Grid, Header } from 'semantic-ui-react';

import styles from './styles.module.scss';

const ResetPasswordPage = ({ match, history }) => {
  const [password, setPassword] = useState('');
  const [confirmationPassword, setConfirmationPassword] = useState('');
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [error, setError] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const [passwordsMatch, setPasswordsMatch] = useState(false);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      if (successMessage) {
        history.push('/');
      }
    }, 3000);
  }, [successMessage]);

  const passwordChanged = data => {
    setPassword(data);
    setError('');
    setIsPasswordValid(true);
  };

  const checkConfirmationPassword = () => {
    const newPassword = password.trim();
    const newConfirmationPassword = confirmationPassword.trim();
    setError('');

    if (newPassword !== newConfirmationPassword) {
      setError('Password from the confirmation field should be the same as from above password field');
    }

    setPasswordsMatch(newPassword === newConfirmationPassword);
  };

  const handleUpdatePassword = async () => {
    if ((!passwordsMatch && !isPasswordValid) || isLoading) {
      return;
    }
    setLoading(true);
    try {
      const { message } = await updatePassword(match.params.token, password);
      setSuccessMessage(message);
    } catch (err) {
      setError(err.message);
      setTimeout(() => history.push('/'), 3000);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center" style={{ display: 'block' }}>
          Update Password
        </Header>
        <Form error={Boolean(error)} name="updatePasswordForm" size="large" onSubmit={handleUpdatePassword}>
          <Segment>
            <p className={styles.formLabel}>
              Enter your new password below, and confirm it, please.
            </p>
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
              error={!isPasswordValid}
              onChange={e => passwordChanged(e.target.value)}
              onBlur={() => setIsPasswordValid(Boolean(password))}
            />
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Confirm Password"
              type="password"
              error={!passwordsMatch && Boolean(error)}
              onChange={e => setConfirmationPassword(e.target.value)}
              onBlur={() => checkConfirmationPassword(Boolean(password))}
            />
            <Message
              error
              content={error}
            />
            {successMessage && (
              <Message
                positive
                content={successMessage}
              />
            )}
            {!successMessage && (
              <Button
                fluid
                primary
                type="submit"
                color="teal"
                size="large"
                loading={isLoading}
                disabled={!(password && passwordsMatch)}
              >
                Update Password
              </Button>
            )}
          </Segment>
        </Form>
      </Grid.Column>
    </Grid>
  );
};

ResetPasswordPage.propTypes = {
  match: PropTypes.objectOf(PropTypes.any),
  history: PropTypes.objectOf(PropTypes.any)
};

ResetPasswordPage.defaultProps = {
  match: undefined,
  history: undefined
};

export default ResetPasswordPage;
