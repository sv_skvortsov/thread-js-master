import React, { useState, useCallback, useRef } from 'react';
import ReactCrop from 'react-image-crop';
import PropTypes from 'prop-types';
import { Image, Icon, Button } from 'semantic-ui-react';

import { getUserImgLink } from 'src/helpers/imageHelper';
import { uploadImage } from '../../../../services/imageService';

import styles from './styles.module.scss';

export default function ProfilePhoto({ user, handleCropImageSave }) {
  const [image, setImage] = useState(undefined);
  const [isCropping, setIsCropping] = useState(false);
  const [isUploading, setIsUploading] = useState(false);
  const [completedCrop, setCompletedCrop] = useState(false);
  const [cropConfirmed, setCropConfirmed] = useState(false);
  const [crop, setCrop] = useState({
    aspect: 1,
    width: 100,
    height: 100
  });
  const imgRef = useRef(null);

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      setIsUploading(false);
      setCropConfirmed(false);

      // Reset crop settings to initial value
      setCrop({
        ...crop,
        x: 0,
        y: 0,
        width: 100,
        height: 100
      });
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  function getCroppedImg(cropObj) {
    const imageFile = imgRef.current;
    const canvas = document.createElement('canvas');
    const scaleX = imageFile.naturalWidth / imageFile.width;
    const scaleY = imageFile.naturalHeight / imageFile.height;
    canvas.width = cropObj.width;
    canvas.height = cropObj.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(
      imageFile,
      cropObj.x * scaleX,
      cropObj.y * scaleY,
      cropObj.width * scaleX,
      cropObj.height * scaleY,
      0,
      0,
      cropObj.width,
      cropObj.height
    );

    return new Promise(resolve => canvas.toBlob(blob => resolve(blob), 'image/jpeg', 1));
  }

  const handleCropImage = async () => {
    setIsCropping(true);
    const croppedImg = await getCroppedImg(crop);
    const { id: imageId, link: imageLink } = await uploadImage(croppedImg);

    setIsCropping(false);
    setImage({ imageId, imageLink });
    setCropConfirmed(true);

    // Put the selection area on cropped image
    setCrop({
      ...crop,
      x: -1,
      y: -1,
      width: 210,
      height: 210
    });

    handleCropImageSave({ imageId, imageLink });
  };

  return (
    <>
      <div className={styles.profilePhotoWrapper}>
        {image?.imageLink ? (
          <ReactCrop
            src={image?.imageLink}
            crop={crop}
            maxWidth="200"
            maxHeight="200"
            onChange={c => setCrop(c)}
            onComplete={() => setCompletedCrop(true)}
            onImageLoaded={onLoad}
            crossorigin="anonymous"
            disabled={cropConfirmed}
          />
        ) : <Image centered src={getUserImgLink(user.image)} size="medium" circular />}
      </div>
      {completedCrop && !cropConfirmed ? (
        <Button
          icon
          labelPosition="left"
          as="label"
          color="blue"
          loading={isCropping}
          disabled={isCropping}
          onClick={handleCropImage}
        >
          <Icon name="crop" />
          Crop the Photo
        </Button>
      ) : (
        <Button
          icon
          labelPosition="left"
          as="label"
          color="teal"
          loading={isUploading}
          disabled={isUploading}
        >
          <Icon name="image" />
          Select New Photo
          <input name="image" type="file" accept="image/*" onChange={handleUploadFile} hidden />
        </Button>
      )}
    </>
  );
}

ProfilePhoto.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  handleCropImageSave: PropTypes.func.isRequired
};

ProfilePhoto.defaultProps = {
  user: {}
};
