import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Grid,
  Input,
  Icon,
  Button
} from 'semantic-ui-react';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { updateProfilePhoto } from '../../services/imageService';
import { updateUsername, updateUserImageId, updateUserStatus } from '../../services/authService';
import { setUser } from './actions';

import styles from './styles.module.scss';
import 'react-image-crop/lib/ReactCrop.scss';
import ProfilePhoto from './components/ProfilePhoto/ProfilePhoto';

const Profile = ({ user, dispatch }) => {
  const [editUsername, setEditUsername] = useState(false);
  const [editStatus, setEditStatus] = useState(false);
  const [username, setUsername] = useState(user.username);
  const [status, setStatus] = useState(user.status || '');
  const [usernameError, setUsernameError] = useState(null);
  const [image, setImage] = useState(undefined);
  const [cropConfirmed, setCropConfirmed] = useState(false);
  const usernameInputRef = useRef(null);
  const statusInputRef = useRef(null);

  useEffect(() => {
    if (!editStatus && usernameInputRef && usernameInputRef.current) {
      usernameInputRef.current.focus();
    }

    if (!editUsername && statusInputRef && statusInputRef.current) {
      statusInputRef.current.focus();
    }
  }, [editUsername, editStatus]);

  const handleUsernameSave = async () => {
    setEditUsername(false);
    const [err, res] = await updateUsername(username.trim());

    if (err) {
      setUsernameError(err);
      NotificationManager.error(err);
      return <NotificationContainer />;
    }

    dispatch(setUser(res));
    setUsernameError(null);

    return false;
  };

  const handlePhotoSave = async () => {
    const imageId = user.imageId || image.imageId;
    const { id, link } = await updateProfilePhoto(imageId, image.imageLink);
    const updatedUser = { ...user };

    if (!user.imageId) {
      await updateUserImageId(user.userId, imageId);
      updatedUser.image = { id, link };
      updatedUser.imageId = id;
    }

    updatedUser.image.link = link;
    setCropConfirmed(false);
    dispatch(setUser(updatedUser));
  };

  const handleStatusSave = async () => {
    setEditStatus(false);
    await updateUserStatus(status.trim());
    return false;
  };

  const handleSave = () => {
    let entities = '';

    if (editUsername) {
      entities += 'username, ';
      handleUsernameSave();
    }

    if (cropConfirmed) {
      entities += 'photo';
      handlePhotoSave();
    }

    if (editStatus) {
      entities += 'status';
      handleStatusSave();
    }

    NotificationManager.info(`Profile ${entities} successfully updated`);
    return <NotificationContainer />;
  };

  const onImageSave = imageObj => {
    setImage(imageObj);
    setCropConfirmed(true);
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <ProfilePhoto user={user} handleCropImageSave={onImageSave} />
        <br />
        <br />
        <div className={styles.usernameInputWrapper}>
          <Input
            ref={usernameInputRef}
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            disabled={!editUsername}
            value={username}
            onChange={e => setUsername(e.target.value)}
            error={!!usernameError}
          />
          <Icon
            name="pencil"
            className={styles.editIcon}
            onClick={() => { setEditUsername(true); setEditStatus(false); }}
          />
        </div>
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <div className={styles.usernameInputWrapper}>
          <Input
            ref={statusInputRef}
            icon="bullhorn"
            iconPosition="left"
            placeholder="Set your status"
            type="email"
            disabled={!editStatus}
            value={status}
            onChange={e => setStatus(e.target.value)}
          />
          <Icon
            name="pencil"
            className={styles.editIcon}
            onClick={() => { setEditUsername(false); setEditStatus(true); }}
          />
        </div>
        <br />
        {(editUsername || cropConfirmed || editStatus) && (
          <Button
            compact
            color="blue"
            onClick={handleSave}
          >
            Save
          </Button>
        )}
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  dispatch: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

export default connect(
  mapStateToProps
)(Profile);
