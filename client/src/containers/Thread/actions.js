import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

const handleReactionOnPost = async (postId, isLike, dispatch, getRootState) => {
  const { id } = await postService.reactOnPost(postId, isLike);
  const diff = id ? 1 : -1; // if ID exists then the post was reacted, otherwise - reaction was removed
  const reactionCount = isLike ? 'likeCount' : 'dislikeCount';

  const mapReaction = post => ({
    ...post,
    [reactionCount]: Number(post[reactionCount]) + diff // diff is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReaction(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapReaction(expandedPost)));
  }
};

export const likePost = postId => async (dispatch, getRootState) => {
  handleReactionOnPost(postId, true, dispatch, getRootState);
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  handleReactionOnPost(postId, false, dispatch, getRootState);
};

export const updatePostBody = (postId, body, edited) => async (dispatch, getRootState) => {
  const updatedPost = await postService.updatePost(postId, body, edited);
  const { posts: { posts } } = getRootState();

  const postBeforeUpdate = posts.find(({ id }) => id === updatedPost.id);

  const mapPosts = posts.map(post => {
    if (post.id === updatedPost.id) {
      return { ...postBeforeUpdate, ...updatedPost };
    }
    return post;
  });

  dispatch(setPostsAction(mapPosts));
};

export const deletePostById = postId => async (dispatch, getRootState) => {
  await postService.deletePost(postId);
  const { posts: { posts, expandedPost } } = getRootState();

  const updatedPostsList = posts.filter(({ id }) => id !== postId);

  dispatch(setPostsAction(updatedPostsList));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(null));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = (commentId, body, edited) => async (dispatch, getRootState) => {
  const { id } = await commentService.updateComment(commentId, body, edited);
  const updatedComment = await commentService.getComment(id);

  const { posts: { expandedPost } } = getRootState();

  const mapComments = post => {
    const postComments = post.comments?.map(c => (id === c.id ? updatedComment : c));

    return {
      ...post,
      comments: postComments
    };
  };

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const loadExpendedPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(setExpandedPostAction(post));
};

export const deleteCommentById = commentId => async (dispatch, getRootState) => {
  await commentService.deleteComment(commentId);
  const { posts: { posts, expandedPost } } = getRootState();

  const filteredComments = expandedPost.comments?.filter(({ id }) => id !== commentId);

  const mapPosts = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: filteredComments
  });

  const updatedPosts = posts.map(post => (post.id !== expandedPost.id
    ? post
    : mapPosts(post)));

  dispatch(setPostsAction(updatedPosts));
  dispatch(setExpandedPostAction(mapPosts(expandedPost)));
};

const handleReactionOnComment = async (commentId, isLike, dispatch, getRootState) => {
  const { id } = await commentService.reactOnComment(commentId, isLike);
  const diff = id ? 1 : -1; // if ID exists then the comment was reacted, otherwise - reaction was removed
  const reactionCount = isLike ? 'likeCount' : 'dislikeCount';

  const { posts: { expandedPost } } = getRootState();

  const updatedComment = comment => ({
    ...comment,
    [reactionCount]: Number(comment[reactionCount]) + diff
  });
  const mapComments = post => {
    const postComments = post.comments?.map(c => (commentId === c.id ? updatedComment(c) : c));

    return {
      ...post,
      comments: postComments
    };
  };

  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  handleReactionOnComment(commentId, true, dispatch, getRootState);
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  handleReactionOnComment(commentId, false, dispatch, getRootState);
};
