/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import io from 'socket.io-client';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  updatePostBody,
  deletePostById
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  updatePostBody: updatePostText,
  deletePostById: softDeletePost
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideMyPosts, setHideMyPosts] = useState(false);
  const [showLikedByMe, setShowLikedByMe] = useState(false);

  const updatePost = (postId, closeExpanded) => {
    postsFilter.from = 0;
    load(postsFilter);
    if (expandedPost && !closeExpanded) {
      toggle(postId);
    }
  };

  useEffect(() => {
    const { REACT_APP_SOCKET_SERVER: address } = process.env;
    const socket = io(address);

    socket.on('reaction', updatePost);
    socket.on('post_deleted', updatePost);
    socket.on('post_updated', id => updatePost(id, false));

    return () => {
      socket.close();
    };
  }, [expandedPost]);

  const toggleFilter = (toggleAction, actionName) => {
    postsFilter.userId = toggleAction ? undefined : userId;
    postsFilter[actionName] = !toggleAction || undefined;

    // If at least one toggle is active – save userId
    if (showLikedByMe || !hideMyPosts || !showOwnPosts) {
      postsFilter.userId = userId;
    }

    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.hideMyPosts = undefined;

    toggleFilter(showOwnPosts, 'showOwnPosts');
  };

  const toggleHideMyPosts = () => {
    setHideMyPosts(!hideMyPosts);
    toggleFilter(hideMyPosts, 'hideMyPosts');
  };

  const toggleShowLikedByMe = () => {
    setShowLikedByMe(!showLikedByMe);
    toggleFilter(showLikedByMe, 'showLikedByMe');
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.filtersWrapper}>
        <div>
          <div className={styles.toolbar}>
            <Checkbox
              toggle
              label="Show only my posts"
              checked={showOwnPosts}
              disabled={hideMyPosts}
              onChange={toggleShowOwnPosts}
            />
          </div>
          <div className={styles.toolbar}>
            <Checkbox
              toggle
              label="Hide my posts"
              checked={hideMyPosts}
              disabled={showOwnPosts}
              onChange={toggleHideMyPosts}
            />
          </div>
        </div>
        <div className={styles.toolbar}>
          <Checkbox
            toggle
            label="Liked by me"
            checked={showLikedByMe}
            onChange={toggleShowLikedByMe}
          />
        </div>
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            updatePost={updatePostText}
            deletePost={softDeletePost}
            currentUserId={userId}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost userId={userId} sharePost={sharePost} />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  updatePostBody: PropTypes.func.isRequired,
  deletePostById: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  updatePostBody,
  deletePostById
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
